package Controller;

import com.sun.jna.Library;;

public interface FuncoesDll extends Library {
	
	public int GeraNumeroSessao();
	public String Base64ToAscii();
	public String AtivarSat(int iNumSessao, int iSubCmd, String pcCodAtivacao, String pcCnpj, int icF);
	public String EnviarDadosVenda(int iNumSessao, String pcCodAtivacao, String pcDadosVenda);
	public String CancelarUltimaVenda(int iNumSessao, String pcCodAtivacao, String pcChaveCfe, String cDadosCanc);
	public String ConsultarSAT(int iNumSessao);
	public String TesteFimAFim(int iNumSessao, String pcCodAtivacao, String pcDadosVenda);
	public String ConsultarStatusOperacional(int iNumSessao, String pcCodAtivacao);
	public String ConsultarNumeroSessao(int iNumSessao, String pcCodAtivacao, int iNumSessaoConsultada);
	public String ConfigurarInterfaceDeRede(int iNumSessao, String pcCodAtivacao, String pcDadosConfiguracao);
	public String AssociarAssinatura(int iNumSessao, String pcCodAtivacao, String pcCnpj, String pcAssinaturaCnpjs);
	public String AtualizarSoftwareSAT(int iNumSessao, String pcCodAtivacao);
	public String ExtrairLogs(int iNumSessao, String pcCodAtivacao);
	public String BloquearSAT(int iNumSessao, String pcCodAtivacao);
	public String DesbloquearSAT(int iNumSessao, String pcCodAtivacao);
	public String TrocarCodigoDeAtivacao(int iNumSessao, String pcCodAtivacao, int iOpcao, String pcNovoCod, String pcConfirmaNovoCod);
	
}

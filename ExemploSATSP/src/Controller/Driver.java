package Controller;

import java.util.Scanner;
import java.io.*;
import com.sun.jna.Native;

public class Driver {
	
	public FuncoesDll dll;
	String retorno;
	int i;

	public void Driver() {  // CHAMANDO E APONTANDO A DLL DO EQUIPAMENTO.
		
		try {
			dll = (FuncoesDll) Native.loadLibrary("dllsat.dll", FuncoesDll.class);
			System.out.println("DLL Carregada");
		} catch (Exception e) {
			System.out.println("Erro.");
		}
	}
	public int MenuPrincipal() throws IOException {
		System.out.println("____________________________\n");
		System.out.println("- E X E M P L O      S A T -");
		System.out.println("____________________________\n\n\n");
		System.out.println("� [1] CONSULTAR SAT\n");
		System.out.println("� [2] EXTRAIR LOG\n");
		System.out.println("� [3] EXTRAIR ESTADO OPERACIONAL\n");
		System.out.println("� [4] BLOQUEAR SAT\n");
		System.out.println("� [5] TESTE FIM A FIM\n");
		System.out.println("� [6] DESBLOQUEAR SAT\n");
		System.out.println("� [7] ATUALIZAR SAT\n");
		System.out.println("� [8] TROCAR C�DIGO DE ATIVA��O\n");
		System.out.println("� [9] VINCULAR APLICATIVO COMERCIAL\n");
		System.out.println("� [10] ATIVAR EQUIPAMENTO\n");
		System.out.println("� [11] CONFIGURAR REDE\n");
		System.out.println("� [12] FINALIZAR PROGRAMA\n");
		
		Scanner digito = new Scanner(System.in);
		int num = digito.nextInt();
		
		switch(num) {
		case 1: ConsultarSAT();
			break;
		case 2: ExtrairLog();
			break;
		case 3: EstadoOperacional();
			break;
		case 4: BloquearSat();
			break;
		case 5: TesteFimaFim();
			break;
		case 6: DesbloquearSat();
			break;
		case 7: AtualizarSat();
			break;
		case 8: TrocarCodigo();
			break;
		case 9: VincularAc();
			break;
		case 10: AtivarSat();
			break;
		case 11: ConfigurarRede();
			break;
		case 12: FinalizarPrograma();
			break;
		}
		return num;
	}
	public void ConsultarSAT() {
		
		i = dll.GeraNumeroSessao();
		retorno = dll.ConsultarSAT(i);
		System.out.println(retorno);
	}
	public void ExtrairLog() throws IOException {
		
		i = dll.GeraNumeroSessao();
		retorno = dll.ExtrairLogs(i, "123456789");
		retorno = dll.Base64ToAscii();
		FileWriter arquivo = new FileWriter("C:\\Users\\george.rocha\\Documents\\An�lises\\log.txt");
		PrintWriter gravararquivo = new PrintWriter(arquivo);
		gravararquivo.printf(retorno);
		arquivo.close();
		
		System.out.println("Foi criado um arquivo no Disco Local C: com o LOG do equipamento.");
		
	}
	public void EstadoOperacional() {
		
		i = dll.GeraNumeroSessao();
		retorno = dll.ConsultarStatusOperacional(i, "123456789");
		String[] ret = retorno.split("[|]");
		System.out.println("\nE  S  T  A  D  O     O  P  E  R  A  C  I  O  N  A  L");
		System.out.println("\nN�mero da Sess�o: "+ret[0]);
		System.out.println("\nC�digo EEEEE: "+ret[1] );
		System.out.println("\nMensagem: "+ret[2]);
		System.out.println("\nC�digo de Refer�ncia: "+ret[3]);
		System.out.println("\nMensagem Sefaz: "+ret[4]);
		System.out.println("\nN�mero de S�rie: "+ret[5]);
		System.out.println("\nTipo de LAN: "+ret[6]);
		System.out.println("\nLAN IP: "+ret[7]);
		System.out.println("\nLAN MAC: "+ret[8]);
		System.out.println("\nLAN MASK: "+ret[9]);
		System.out.println("\nLAN GW: "+ret[10]);
		System.out.println("\nLAN DNS 1: "+ret[11]);
		System.out.println("\nLAN DNS 2: "+ret[12]);
		System.out.println("\nSTATUS LAN: "+ret[13]);
		System.out.println("\nN�vel De Bateria: "+ret[14]);
		System.out.println("\nMem�ria Total: "+ret[15]);
		System.out.println("\nMem�ria em Uso: "+ret[16]);
		System.out.println("\nData & Hora: "+ret[17]);
		System.out.println("\nVers�o do Software: "+ret[18]);
		System.out.println("\nVers�o do Layout: "+ret[19]);
		System.out.println("\nUltimo CFe emitido: "+ret[20]);
		System.out.println("\nPrimeiro CFe emitido: "+ret[21]);
		System.out.println("\nData e Hora da ultima transmiss�o: "+ret[22]);
		System.out.println("\nUltima comunica��o com a SEFAZ: "+ret[23]);
		System.out.println("\nCertificado: "+ret[24]);
		System.out.println("\nVencimento do Certificado: "+ret[25]);
		System.out.println("\nStatus de Opera��o: "+ret[26]);
		System.out.println("\n");
		
	}
	public void BloquearSat() {
		i = dll.GeraNumeroSessao();
		retorno = dll.BloquearSAT(i, "123456789");
		System.out.println(retorno);
	}
	public void TesteFimaFim() {
		i = dll.GeraNumeroSessao();
		String xmltestefimafim = "XML HERE.";
		retorno = dll.TesteFimAFim(i, "123456789", xmltestefimafim);
		System.out.println(retorno);
		
	}
	public void DesbloquearSat() {
		i = dll.GeraNumeroSessao();
		retorno = dll.DesbloquearSAT(i, "123456789");
		System.out.println(retorno);
	}
	public void AtualizarSat() {
		i = dll.GeraNumeroSessao();
		retorno = dll.AtualizarSoftwareSAT(i, "123456789");
		System.out.println(retorno);
	}
	public void TrocarCodigo() {
		i =  dll.GeraNumeroSessao();
		retorno = dll.TrocarCodigoDeAtivacao(i, "123456789", 1, "987654321", "987654321");
		String [] ret = retorno.split("[|]");
		System.out.println("N�mero de Sess�o: "+ret[0]);
		System.out.println("C�digo EEEEE: "+ret[1]);
		System.out.println("Mensagem: "+ret[2]);
		System.out.println("C�digo: "+ret[3]);
		System.out.println("Mensagem SEFAZ: "+ret[4]);
		
	}
	public void VincularAc() {
		i = dll.GeraNumeroSessao();
		String assinatura = "SGR-SAT SISTEMA DE GESTAO E RETAGUARDA DO SAT1671611400017214200166000166";
		retorno = dll.AssociarAssinatura(i, "123456789", "1671611400017214200166000166", assinatura);
		String[] ret = retorno.split("[|]");
		System.out.println("N�mero de Sess�o: "+ret[0]);
		System.out.println("C�digo EEEEE: "+ret[1]);
		System.out.println("Mensagem: "+ret[2]);
		System.out.println("C�digo: "+ret[3]);
		System.out.println("Mensagem SEFAZ: "+ret[4]);
	}
	public void AtivarSat() {
		i = dll.GeraNumeroSessao();
		retorno = dll.AtivarSat(i, 1, "123456789", "14200166000166", 33);
		retorno = dll.Base64ToAscii();
		System.out.println(retorno);
	}
	public void ConfigurarRede() {
		i = dll.GeraNumeroSessao();
		String xmlrede = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n" + 
				"<config>\r\n" + 
				"<tipoInter>ETHE</tipoInter>\r\n" + 
				"<tipoLan>DHCP</tipoLan>\r\n" + 
				"</config>";
		retorno = dll.ConfigurarInterfaceDeRede(i, "123456789", xmlrede);
		
		String[] ret = retorno.split("[|]");
		System.out.println("N�mero de Sess�o: "+ret[0]);
		System.out.println("C�digo EEEEE: "+ret[1]);
		System.out.println("Mensagem: "+ret[2]);
		System.out.println("C�digo: "+ret[3]);
		System.out.println("Mensagem SEFAZ: "+ret[4]);
	}
	public void FinalizarPrograma() {
		System.out.println("Programa Finalizado.");
	}
}
